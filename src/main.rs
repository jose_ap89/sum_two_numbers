//Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
  pub val: i32,
  pub next: Option<Box<ListNode>>
}

impl ListNode {
  #[inline]
  fn new(val: i32) -> Self {
    ListNode {
      next: None,
      val
    }
  }
}
pub fn add_two_numbers(l1: Option<Box<ListNode>>, l2: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
    let mut v1 = to_vec(l1);
    let mut v2 = to_vec(l2);
    if v1.len()<v2.len(){
        for i in 0..v2.len(){
            v1.push(0);
        }
    } else {
        for i in 0..v1.len(){
            v2.push(0);
        }
    }
    let mut res: Option<Box<ListNode>> = None;
    let mut prov = vec![];
    let mut carry = false;
    for (i,j) in v1.iter().zip(v2.iter()) {
        let mut sum = 0;
        if carry {
            sum+= 1;
            carry = false;
        }
        sum = if sum+i+j>9 {
            carry=true;
            sum+i+j-10
        } else {
            carry = false;
            sum+i+j
        };
        prov.push(sum);
    }
    if carry {
        println!("here");
        prov.push(1);
    }
    for i in prov.iter().rev() {
        if let Some(_)= res {
            res = res.unwrap().prepend(*i);
        } else {
            res = Some(Box::new(ListNode::new(*i)));
        }
    }
    res
}

impl ListNode {
    pub fn prepend(self, val: i32) -> Option<Box<Self>> {
        Some(Box::new(Self {
            val,
            next:  Some(Box::new(self))
        }))
    }
}

pub fn to_vec(data: Option<Box<ListNode>>) -> Vec<i32> {
    let mut v = vec![];
    let mut data = data;
    while let Some(b) = data {
        v.push(b.val);
        data = b.next;
    }
    v
}

fn main() {
    println!("Hello, world!");
}
